#!/bin/bash

# Copyright 2022, Timothy Redaelli <timothy.redaelli@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License at <http://www.gnu.org/licenses/> for
# more details.

set -eo pipefail

echo "nameserver $TOR_NODE_IP" > /etc/resolv.conf

echo -n "Getting VPNBOOK username, password and configuration..."
curl -s --socks5-hostname $TOR_NODE_IP:9050 https://nitter.net/vpnbook | \
	sed -n -e 's/.*Username: \([a-z0-9]*\)\s*Password: \([a-z0-9]*\).*/\1\n\2/p' | \
        head -n 2 > /tmp/userpass
curl -sL --socks5-hostname $TOR_NODE_IP:9050 \
	https://www.vpnbook.com/free-openvpn-account/VPNBook.com-OpenVPN-DE4.zip > \
	/tmp/VPNBook.com-OpenVPN-DE4.zip
unzip -q -o -d /tmp /tmp/VPNBook.com-OpenVPN-DE4.zip vpnbook-de4-tcp80.ovpn
echo " [OK]"

gosu nobody microsocks &

echo "Connecting to VPNBOOK"
exec openvpn --config /tmp/vpnbook-de4-tcp80.ovpn \
	--redirect-gateway local \
	--user nobody --group nogroup \
	--auth-user-pass /tmp/userpass --socks-proxy $TOR_NODE_IP 9050
