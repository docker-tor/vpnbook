FROM debian:stable-slim
MAINTAINER Timothy Redaelli <timothy.redaelli@gmail.com>

RUN apt-get update -y && DEBIAN_FRONTEND=noninteractive apt-get -o Acquire::Retires=10 install --no-install-recommends -y \
    ca-certificates \
    curl \
    gosu \
    microsocks \
    openvpn \
    unzip && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

COPY startup.sh /usr/local/sbin/startup.sh

CMD /usr/local/sbin/startup.sh
